package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

// google drive

type GoogleDriveFile struct {
	Kind           string `json:"kind"`
	Id             string `json:"id"`
	Name           string `json:"name"`
	WebContentLink string `json:"webContentLink"`
}

type GoogleDriveRes struct {
	Kind  string            `json:"kind"`
	Files []GoogleDriveFile `json:"files"`
}

func head(ss []string) (string, []string) {
	for i, v := range ss {
		if v != "" {
			return v, ss[(i + 1):]
		}
	}
	return "", nil
}

// 1) get folder id from web url
// 2) share that folder to anyone to read
// 3) create proejct at https://console.cloud.google.com/
// 4) from APIs & Services add Google Drive API
// 5) from APIs & Services / Google Drive API / Credentials / Create Credential / API key
func GoogleDriveList(client *http.Client, token string, id string) ([]GoogleDriveFile, error) {
	url := "https://www.googleapis.com/drive/v3/files?q=%27" + id + "%27%20in%20parents%20and%20trashed%20%3D%20false&fields=files(kind,id,name,mimeType,webContentLink)&key=" + token
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	res := new(GoogleDriveRes)
	json.NewDecoder(resp.Body).Decode(res)
	return res.Files, nil
}

func GoogleDrivePath(client *http.Client, token string, id string, path string) (*GoogleDriveFile, error) {
	dd, err := GoogleDriveList(client, token, id)
	if err != nil {
		return nil, err
	}
	pp := strings.Split(path, "/")
	p, pp := head(pp)
	for _, d := range dd {
		if d.Name == p {
			if len(pp) == 0 {
				return &d, nil
			} else {
				return GoogleDrivePath(client, token, d.Id, strings.Join(pp, "/"))
			}
		}
	}
	return nil, fmt.Errorf("not found")
}

func GoogleDriveLink(client *http.Client, token GoogleDriveToken, path string) (string, error) {
	d, err := GoogleDrivePath(client, token.Token, token.FolderID, path)
	if err != nil {
		return "", err
	}
	return d.WebContentLink, nil
}
